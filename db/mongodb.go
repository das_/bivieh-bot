package db

import (
	"bitbucket.org/Seliba/bivieh-bot/logger"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"math"
	"os"
	"time"
)

// Credentials that are used to
// login to the mongodb database
type Credentials struct {
	Host     string
	Port     string
	Username string
	Password string
}

// User struct respresents a user
// that are stored in the database
type User struct {
	Id    string `json:"id" bson:"_id"`
	Level int64  `json:"level" bson:"level"`
	Xp    int64  `json:"xp" bson:"xp"`
}

var (
	client *mongo.Client
)

// Login connects to the mongodb database
// with the provided credentials
func Login(credentials Credentials) {
	c, err := mongo.NewClient(options.Client().ApplyURI("mongodb://" + credentials.Host + ":" + credentials.Port))
	client = c
	if err != nil {
		logger.Log("Error while connecting to MongoDB...")
		logger.Log("Stopping in 5 seconds...")
		time.Sleep(5 * time.Second)
		os.Exit(0)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		logger.Log("Error while connecting to MongoDB...")
		logger.Log("Stopping in 5 seconds...")
		time.Sleep(5 * time.Second)
		os.Exit(0)
	}
	ctx, cancel = context.WithTimeout(context.Background(), 2*time.Second)
	err = client.Ping(ctx, readpref.Primary())
	defer cancel()
	if err != nil {
		logger.Log("Error while connecting to MongoDB...")
		logger.Log("Stopping in 5 seconds...")
		time.Sleep(5 * time.Second)
		os.Exit(0)
	}
}

// Disconnect interrupts the connection
// to the database
func Disconnect() {
	_ = client.Disconnect(context.Background())
}

// AddXp adds xp to the user in the mongodb
// database and increases the level if
// needed
func AddXp(id string, amount int64) {
	u := User{}
	collection := client.Database("bots").Collection("bivieh")
	f := bson.M{"_id": id}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err := collection.FindOne(ctx, f).Decode(&u)
	if err != nil {
		logger.Log("Creating new entry for User with ID " + id + "...")
		u.Id = id
		u.Level = 1
		u.Xp = amount
	}

	u.Xp = u.Xp + amount
	maxXp := XpToLevelUp(u.Level)
	if u.Xp >= maxXp {
		u.Level = u.Level + 1
		u.Xp = u.Xp - maxXp
	}

	d := bson.D{
		{"$set", bson.D{
			{"level", u.Level},
			{"xp", u.Xp},
		}},
		{"$currentDate", bson.D{
			{"lastModified", true},
		}},
	}
	var _, e = collection.UpdateOne(ctx, f, d, &options.UpdateOptions{Upsert: getTrue()})
	if e != nil {
		fmt.Println(e)
		logger.Log("Error while writing to the database...")
	}
}

// GetUser fetches all information of
// an user from the database and builds
// an User struct instance with it
func GetUser(id string) User {
	u := User{}
	collection := client.Database("bots").Collection("bivieh")
	f := bson.M{"_id": id}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err := collection.FindOne(ctx, f).Decode(&u)
	if err != nil {
		logger.Log("Creating new entry for User with ID " + id + "...")
		u.Id = id
		u.Level = 1
		u.Xp = 0
	}
	return u
}

// XpToLevelUp returns the max xp amount
// for the provided level
func XpToLevelUp(l int64) int64 {
	return int64(math.Sqrt(float64(l)) * 25)
}

func getTrue() *bool {
	b := true
	return &b
}
