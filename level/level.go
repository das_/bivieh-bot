package level

import (
	"bitbucket.org/Seliba/bivieh-bot/db"
	"github.com/bwmarrin/discordgo"
	"time"
)

var (
	lastMessageTimes = make(map[string]time.Time)
)

func messageCreated(u *discordgo.User, t time.Time) {
	if _, ok := lastMessageTimes[u.ID]; !ok {
		resetCooldown(u, t)
		giveXp(u, 5)
		return
	}
	if lastMessageTimes[u.ID].Unix() > time.Now().Unix() {
		return
	}
	resetCooldown(u, t)
	giveXp(u, 5)
}

func giveXp(u *discordgo.User, a int64) {
	db.AddXp(u.ID, a)
}

func resetCooldown(u *discordgo.User, t time.Time) {
	m, _ := time.ParseDuration("1m")
	nt := t.Add(m)
	lastMessageTimes[u.ID] = nt
}
