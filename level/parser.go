package level

import (
	"github.com/bwmarrin/discordgo"
	"strings"
	"time"
)

var (
	commands = make(map[string]CommandHandler)
)

// CommandHandler represents a command
type CommandHandler interface {
	onCommand(s *discordgo.Session, m *discordgo.MessageCreate)
}

// MessageCreateListener gets executed when a message is created,
// used to give xp and execute commands
func MessageCreateListener(s *discordgo.Session, m *discordgo.MessageCreate) {
	//Return if the sender is a bot
	if m.Author.Bot {
		return
	}

	//Check if the sender executed an registered command and if so execute it
	c := strings.Split(m.Message.Content, " ")[0]
	if _, ok := commands[c]; ok {
		commands[c].onCommand(s, m)
		return
	}
	messageCreated(m.Author, time.Now())
}

// RegisterCommand registers a command of the bot
func RegisterCommand(c string, h CommandHandler) {
	//Put the command in the command map
	commands[c] = h
}
