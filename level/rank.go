package level

import (
	"bitbucket.org/Seliba/bivieh-bot/image"
	"bitbucket.org/Seliba/bivieh-bot/logger"
	"fmt"
	"github.com/bwmarrin/discordgo"
)

// RankCommand represents the !rank command
type RankCommand struct{}

func (r RankCommand) onCommand(s *discordgo.Session, m *discordgo.MessageCreate) {
	fmt.Println(m.Author.Avatar)
	return
	_, e := s.ChannelMessageSendComplex(
		m.ChannelID,
		&discordgo.MessageSend{
			Content: "",
			Embed:   nil,
			Tts:     false,
			Files:   nil,
			File:    image.GetFile(*m.Author),
		},
	)
	if e != nil {
		logger.Log("Error while sending message!")
	}
}
