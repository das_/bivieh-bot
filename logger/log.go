package logger

import (
	"fmt"
	"os"
	"time"
)

// Log prints a message to the console
// and writes it to the log file
func Log(s string) {
	t := time.Now()
	formatted := fmt.Sprintf("[%d-%02d-%02d %02d:%02d:%02d] ",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())
	fmt.Println(formatted + s)

	//Write to the log file
	if _, err := os.Stat("log.txt"); os.IsNotExist(err) {
		os.Create("log.txt")
	}
	f, err := os.OpenFile("log.txt", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	if _, err = f.WriteString(formatted + s + "\n"); err != nil {
		panic(err)
	}
}
