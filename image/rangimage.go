package image

import (
	"bitbucket.org/Seliba/bivieh-bot/db"
	"bitbucket.org/Seliba/bivieh-bot/logger"
	"github.com/bwmarrin/discordgo"
	"github.com/fogleman/gg"
	"io"
	"math"
	"net/http"
	"os"
	"sync"
)

var (
	locker    sync.Mutex
	imageUrls = make([]string, 11)
)

// InitUrls adds all image urls for the
// rank cards to the imageUrls array
func InitUrls() {
	imageUrls[0] = "https://i.imgur.com/MUv4z0C.png"
	imageUrls[1] = "https://i.imgur.com/VLpyt8S.png"
	imageUrls[2] = "https://i.imgur.com/25RmhTZ.png"
	imageUrls[3] = "https://i.imgur.com/VNXzsJq.png"
	imageUrls[4] = "https://i.imgur.com/pw0ESgI.png"
	imageUrls[5] = "https://i.imgur.com/T9SXWjw.png"
	imageUrls[6] = "https://i.imgur.com/gDxfJiR.png"
	imageUrls[7] = "https://i.imgur.com/pOMu2A4.png"
	imageUrls[8] = "https://i.imgur.com/v4XM90F.png"
	imageUrls[9] = "https://i.imgur.com/kAaOwc7.png"
	imageUrls[10] = "https://i.imgur.com/775E7yZ.png"
}

// GetFile creates an image of the
// user containing all information
// about his rank and level
func GetFile(user discordgo.User) *discordgo.File {
	locker.Lock()
	_ = os.Mkdir("temp", 0777)
	_ = os.RemoveAll("temp/" + user.ID)
	_ = os.Mkdir("temp/"+user.ID, 0777)
	a := user.AvatarURL("128")

	e := downloadFile("temp/"+user.ID+"/"+user.ID+"-template.png", getImageURL(user.ID))
	if e != nil {
		println(e)
		logger.Log("Error downloading rank image template!")
		return nil
	}
	e = downloadFile("temp/"+user.ID+"/"+user.ID+"-pb.png", a)
	if e != nil {
		println(e)
		logger.Log("Error downloading user profile picture!")
		return nil
	}

	u := db.GetUser(user.ID)
	_ = u

	/*
		fo, err := truetype.Parse(goregular.TTF)
		face := truetype.NewFace(fo, &truetype.Options{
			Size: 46,
		})

		if err != nil {
			panic(err)
		}
	*/

	png, _ := gg.LoadPNG("temp/" + user.ID + "/" + user.ID + "-template.png")
	pb, _ := gg.LoadPNG("temp/" + user.ID + "/" + user.ID + "-pb.png")
	p := gg.NewContextForImage(png)
	// p.SetFontFace(face)
	// p.SetRGB(255, 165, 0)
	// p.Clear()
	// p.SetRGB(1, 1, 1)
	p.DrawImage(pb, 300, 55)
	p.DrawStringAnchored(user.ID, 300, 200, 0.5, 0.5)

	_ = p.SavePNG("temp/" + user.ID + "/" + user.ID + ".png")

	f, err := os.Open("temp/" + user.ID + "/" + user.ID + ".png")
	if err != nil {
		logger.Log("Error while reading rank picture...")
		return &discordgo.File{}
	}
	defer locker.Unlock()
	return &discordgo.File{
		Name:   user.ID + ".png",
		Reader: f,
	}
}

// downloadFile will download a url to a local file.
func downloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func getImageURL(id string) string {
	u := db.GetUser(id)
	currentXp := float64(u.Xp)
	if currentXp == 0 {
		return imageUrls[0]
	}
	maxXp := float64(db.XpToLevelUp(u.Level))
	xpProportion := (currentXp / maxXp) * 10
	logger.Log("Creating image for " + id + "...")
	logger.Log("")
	return imageUrls[int(math.Round(xpProportion))]
}
