package main

import (
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/Seliba/bivieh-bot/db"
	"bitbucket.org/Seliba/bivieh-bot/image"
	"bitbucket.org/Seliba/bivieh-bot/level"
	"bitbucket.org/Seliba/bivieh-bot/logger"
	"github.com/bwmarrin/discordgo"
)

var (
	authToken string
)

func main() {
	log("Bot starting up...")

	//Read the token used for authentication purposes from the token file
	readAuthToken()

	//Create Discord session
	DiscordSession, err := discordgo.New("Bot " + authToken)
	if err != nil {
		log("Error while creating Discord session!")
		log("Stopping in 5 seconds...")
		time.Sleep(5 * time.Second)
		os.Exit(0)
	}

	//Attach event handler to the session
	DiscordSession.AddHandler(level.MessageCreateListener)

	//Init the image generation
	image.InitUrls()

	//Register commands
	level.RegisterCommand("!rank", new(level.RankCommand))

	//Login into the database
	db.Login(db.Credentials{
		Host: "localhost",
		Port: "27017",
	})

	//Open connection to Discord
	err = DiscordSession.Open()
	if err != nil {
		log("Error while login!")
		log("Stopping in 5 seconds...")
		time.Sleep(5 * time.Second)
		os.Exit(0)
	}

	//Update the Discord status
	err = DiscordSession.UpdateStatus(0, "mit kleinen Würstchen")
	if err != nil {
		log("Error while setting status")
	}

	// Wait here until CTRL-C or other term signal is received.
	log("Bot is now running. Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session and disconnect from the database
	_ = DiscordSession.Close()
	db.Disconnect()
}

func readAuthToken() {
	if _, err := os.Stat("token.txt"); os.IsNotExist(err) {
		err := ioutil.WriteFile("token.txt", []byte("YOUR_TOKEN_HERE"), 0644)
		if err != nil {
			log("Error while writing to file, aborting...")
			os.Exit(-1)
		}
		log("Token not found, please enter your token in the token.txt file!")
		log("Stopping in 5 seconds...")
		time.Sleep(5 * time.Second)
		os.Exit(0)
	} else {
		tempAuthToken, err := ioutil.ReadFile("token.txt")
		authToken = string(tempAuthToken)
		if err != nil {
			log("Error while reading from token.txt, aborting...")
			os.Exit(-1)
		}
		_ = authToken
	}
}

func log(s string) {
	logger.Log(s)
}
