# Bivieh-Bot

[![CircleCI](https://circleci.com/bb/Seliba/bivieh-bot/tree/master.svg?style=svg)](https://circleci.com/bb/Seliba/bivieh-bot/tree/master) [![Go Report Card](https://goreportcard.com/badge/bitbucket.org/Seliba/bivieh-bot)](https://goreportcard.com/report/bitbucket.org/Seliba/bivieh-bot)

A simple Discord bot which adds a level system to Discord servers written in Go.

## Installation

1. [Get the newest version from CircleCI for your operating system](https://circleci.com/bb/Seliba/bivieh-bot) (the bot is currently only available for Linux and Windows amd64). Files from the [master branch](https://circleci.com/bb/Seliba/bivieh-bot/tree/master) should be much more stable.
2. Start the bot and enter your discord bot authentication token in the created token.txt. 
An explanation of how to create an application and obtain its token can be found [here](https://github.com/Chikachi/DiscordIntegration/wiki/How-to-get-a-token-and-channel-ID-for-Discord). 
3. Restart the bot, it should start successfully.

## Constributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
More information how to constribute to this project can be found in the [CONSTRIBUTING.md](https://bitbucket.org/Seliba/bivieh-bot/src/master/CONSTRIBUTING.md).